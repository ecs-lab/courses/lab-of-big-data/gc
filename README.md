# Green Computing (aa. 2023-2024) [Completed]
## Big Data Analysis: Profiling HPC Power and Tracking CO2 Emissions

![Big Data Analysis: Profiling HPC Power and Tracking CO2 Emissions](images/gc.png)

### LAB OBJECTIVE​

Explore the world of Big Data Analysis in the context of high-performance computing (HPC), with a specific focus on two important aspects: power profiling and tracking CO2 emissions.​

### BACKGROUND​

Sustainable computing is increasingly vital in our digital era, especially within HPC systems. To achieve this goal, it is crucial to utilize big data analysis of HPC monitoring data.

### Platfrom

Platform for the following steps: Personal Computer (VSCode+JupyterLab), Google Colab, GitHub Codespaces.

### Python (Requirments):

- Basic Syntax: Understand Python's syntax rules, including indentation, comments, variables, and basic data types (integers, floats, strings, booleans).
- Control Flow: Learn about conditional statements (if, elif, else) and loops (for, while) to control the flow of the program.
- Data Structures: Familiarize yourself with common data structures such as lists, tuples, dictionaries, and sets, and understand their properties and methods.
- Functions: Learn how to define and use functions to modularize code and promote code reuse. Topics include function definition, parameters, return values, and scope.
- File Handling: Understand how to read from and write to files using Python's built-in file handling capabilities.
- Exception Handling: Learn about try-except blocks for handling exceptions and errors within Python programs.
- Modules and Packages: Understand how to organize code into modules and packages, and how to import and use functionality from external modules.
- Debugging and Testing: Familiarize yourself with debugging techniques and tools available in Python, and understand the importance of writing test cases to ensure code correctness.
- Object-Oriented Programming (OOP): Introduce the principles of OOP including classes, objects, inheritance, encapsulation, and polymorphism.
- Virtual Environments: Learn how to create and manage virtual environments to isolate project dependencies and avoid conflicts.

#### Jupyter Notebook and JupyterLab:
Jupyter Notebook and JupyterLab are web-based interactive computing environments that allow users to create and share documents containing live code, equations, visualizations, and narrative text. Both are part of the Jupyter Project, an open-source project that originated from the IPython project.

*I suggest using Jupyter Notebook or JupyterLab for the following steps. If you are not familiar with these tools, I recommend learning about them before proceeding. (Check the YouTube for tutorials.)*

### Let's talk with ChatGPT:

Spend an hour chatting with AI to understand the general idea about the following:
- What is Time Series Data and its characteristics?
- What is Time Series Data Analysis?
- What is Big Data Analysis?

### Pandas (DataFrames):

Visit the Pandas website to learn about the Pandas data frame.
https://pandas.pydata.org/docs/user_guide/timeseries.html
- Can you create a Pandas DataFrame with three columns {C1, C2, C3}? The rows should represent datetime or timestamp values starting from January 1, 2024 and ending on February 1, 2024. The timestamps or range should be generated with a daily frequency (`freq="D"`). Also, please include random data for these columns.

### Open Data (M100 ExaData):

https://www.nature.com/articles/s41597-023-02174-3 This paper discusses the monitoring data of a large-scale data center, specifically a high-performance computing system. Please review it quickly to understand the dataset and its various components. Focus more on the git repository readme than the paper itself.

Action Items:
- Where is the dataset located? Can you provide a link and access to the dataset? Try to download a small chunk of the dataset.
- What is the format of the dataset? Could you use AI/ChatGPT to generate Python code that reads the downloaded data into a Pandas DataFrame and view the contents of the dataset?
- Visit the Git repository and review the README to identify which parts of the dataset are about the data center's power consumption. Then, create a list of metrics that you think are related to power consumption.
- Could you download one month's data and systematically extract a specific part of the power dataset? Specifically, focus on three different power consumption metrics. You might need to learn more about the Parquet format.
- Calculate the mean, median, standard deviation, maximum, and minimum for each of the three metrics in the dataset.
- Create a line plot for a metric where the x-axis represents datetime and the y-axis represents the power value.
- Add a moving average with a one-day window to the same plot. (You can refer to the Pandas website or AI Chat for information on moving averages.)

### Open Data (Electricity Consumption and Carbon Footprint Analysis):

[Carbon Intensity - Low Carbon Percentage - Renewable Percentage](https://www.electricitymaps.com/data-portal/italy)


[Energy source composition](https://transparency.entsoe.eu/generation/r2/actualGenerationPerProductionType/show?name=&defaultValue=false&viewType=GRAPH&areaType=BZN&atch=false&datepicker-day-offset-select-dv-date-from_input=D&dateTime.dateTime=06.03.2024+00:00|CET|DAYTIMERANGE&dateTime.endDateTime=06.03.2024+00:00|CET|DAYTIMERANGE&area.values=CTY|10YIT-GRTN-----B!BZN|10Y1001A1001A73I&productionType.values=B01&productionType.values=B02&productionType.values=B03&productionType.values=B04&productionType.values=B05&productionType.values=B06&productionType.values=B07&productionType.values=B08&productionType.values=B09&productionType.values=B10&productionType.values=B11&productionType.values=B12&productionType.values=B13&productionType.values=B14&productionType.values=B20&productionType.values=B15&productionType.values=B16&productionType.values=B17&productionType.values=B18&productionType.values=B19&dateTime.timezone=CET_CEST&dateTime.timezone_input=CET+(UTC+1)+/+CEST+(UTC+2))

### Open Data and Open Source Code:

The goal of this step is to practice how to find and utilize open data and open-source code for research purposes.
- Please create a list of links to (i) open data sources, (ii) code, or Git repositories related to the carbon footprint of data centers or a carbon tracker for data centers.

### Statsmodels:

If you have extra time, consider visiting the following website where you can find statistical tests and many other useful tools.
https://www.statsmodels.org/stable/examples/index.html

### Papers

[Toward Sustainable HPC: Carbon Footprint Estimation and Environmental Implications of HPC Systems](https://arxiv.org/pdf/2306.13177.pdf)

---
## Task 2
Please follow these steps for the next phase of the lab activities.

You can use the dataset found in the shared folder. LBD_2024
However, it's recommended to download the data yourself from the original sources. The locations of these sources can be found in the readme file of the GreenComputing git repository. https://gitlab.com/ecs-lab/courses/lab-of-big-data/gc

### The Power Consumption of Each Node:

https://gitlab.com/ecs-lab/exadata/-/blob/main/documentation/plugins/ipmi.md
psX_input_power  ⇒  Power consumption at the input of power supply n. X. X=0..1
To calculate the total power consumption of each compute node, please add ps0 and ps1 together.
However, the timestamps may vary. For instance, node01 for power consumption April 5, 2022 11:10:20 has two data points collected at slightly different times ps0 was collected at April 5, 2022 11:10:20.002, while ps1 was collected at April 5, 2022 11:10:20.042.
How can we combine these two power consumption values to calculate the total power consumption?

A simple answer is; we can either cancel the milliseconds or use the moving average on both P0 and P1 with the same sapmling/frequency rate. However, we should first determine the sampling rate.

What is the sampling rate for the power consumption P0 and P1 of the computing nodes? (is it 20 seconds?)

Apply the moving average to the power dataset with 20 second freq (check documents of Pandas for moving average), then you will have three data points per minute. For example, there's a data point at the zero-second, followed by new data points at the 20 and 40-second.

In the dataset, we also have a metric called  total_power, which represents the total power consumption of nodes. This value should be very close to the sum of ps0 and ps1. We've gone through these steps to show you how to deal with this kind of situation, which you will often encounter in data analysis :)

### How to Calculate Energy from Power Consumption:

How can we convert power consumption to energy in a Pandas dataframe? (The .cumsum() function in Pandas might be useful. Check the Pandas documentation for more information.) Or ask from ChatGPT something like

”I have a pandas dataframe with a column that represents the power consumption of a system, sampled every 20 seconds. I would like to know how to compute the energy for each sample.”

### Calculated the Operational Carbon Footprint

(For more information, refer to Section 2.2, "Operational Carbon Footprint Characterization" of the attached paper. https://gitlab.com/ecs-lab/courses/lab-of-big-data/gc#papers)

The operational carbon footprint is defined when computing workloads are operating on the system.

It can be calculated using the carbon intensity of the power plant that powers the system (Isys , unit: gCO2 /kWh) and the system’s operational energy (Eop , unit: kWh).

For this step, it is not necessary to consider the PUE power effectiveness.

Cop = Isys*Eop

Isys ⇒ depends on the time and the geographical location (Emilia Romagna  Electricity Zone Name: North Italy) You can extract data from: "https://www.electricitymaps.com/data-portal/italy#data-portal-form"

Please calculate and create a line plot for the Cop of 20 computing nodes from one randomly selected M100 rack. Then, please calculate the Cop for the different racks. Plot a line chart where the x-axis represents the date time and the y-axis represents the Cop. We should have 49 line charts, each representing one rack. If the plot is unreadable with 49 lines, please generate multiple plots, each with a limited number of line plots.

---

## Task 3

### Moving Average (MA) 

Is a widely used technique in time series analysis to smooth out short-term fluctuations and highlight longer-term trends or cycles. It works by calculating the average of data points over a specific number of past periods. 

Please use the Pandas documentation and try to calculate the moving average using df.rolling(window=window_size).mean(). Use window sizes of 6 hours, 12 hours, 24 hours, one week, and one month. Generate line plots where the x-axis represents time and the y-axis represents the value of the moving average. Please calculate the moving average for the power consumption of a single compute node and carbon intensity.
      Considerations

            Trend Analysis: can you identify any trends in the moving average of data?

            Smoothing: Moving averages help in smoothing out the data to better understand the underlying trend or pattern.

            Seasonality: Do you observe any seasonality in the moving averages plots?

 

### Seasonal-Trend decomposition

Please study the following link for Seasonal-Trend decomposition and apply the same to your dataset for individual node power consumption and carbon intensity.

https://www.statsmodels.org/stable/examples/notebooks/generated/stl_decomposition.html

## Contact
- Prof.Andrea Bartolini (a.bartolini@unibo.it)
- Junaid Ahmed Khan - Tutor (junaidahmed.khan@unibo.it)
- Mohsen Seyedkazemi Ardebili (mohsen.seyedkazemi@unibo.it) => Viale Carlo Pepoli, 3/1, 40123 Bologna BO Multitherman Lab (Please schedule an appointment through email before your coming.)

